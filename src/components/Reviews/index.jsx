import announcements from "../data/announcements";
import cls from "./reviews.module.scss";
import person1 from "../../assets/images/person/jennifer-geiger-headshot-purple-square.webp";
import person2 from "../../assets/images/person/mike-hanley-headshot-purple-square.webp";
import person3 from "../../assets/images/person/brian-normile-headshot-square-purple.webp";
import Card from "./card";

export default function Reviews() {
  const experts = [
    {
      img: <img alt="person-img" src={person1} />,
      name: "Duis",
      profession: "reprehenderit",
    },
    {
      img: <img alt="person-img" src={person2} />,
      name: "Excepteur",
      profession: "voluptate",
    },
    {
      img: <img alt="person-img" src={person3} />,
      name: "Labore",
      profession: "deserunt",
    },
    {
      img: <img alt="person-img" src={person3} />,
      name: "Labore",
      profession: "deserunt",
    },
  ];
  const cards = announcements().map((el, index) => {
    return {
      ...el,
      expert: experts[index],
    };
  });
  cards.length = 4
  return (
    <div className={cls.ReviewsCont}>
      <div className={cls.title}>
        <h1>Eng so'nggi avtomobil relizlari va ekspert sharhlari</h1>
      </div>
      <div className={cls.CardsCont}>
        {cards.map((el) => (
          <Card el={el} />
        ))}
      </div>
    </div>
  );
}
