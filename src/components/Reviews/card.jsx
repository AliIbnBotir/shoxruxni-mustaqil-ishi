import cls from "./reviews.module.scss";

export default function Card({ el }) {
  console.log(el.expert);
  return (
    <div className={cls.Card}>
      <div className={cls.imageCont}>{el.img}</div>
      <div className={cls.infoCont}>
        <div>
          <span>EXPERT REVIEWS</span>
          <p>{el.title}</p>
        </div>
        <div className={cls.expertInfoCont}>
          {el.expert.img}
          <div>
            <p>{el.expert.name}</p>
            <span>{el.expert.profession}</span>
          </div>
        </div>
      </div>
    </div>
  );
}
