import cls from "./announcements.module.scss";
export default function Card({ el }) {
  return (
    <div className={cls.Card}>
      <div className={cls.imageCont}>{el.img}</div>
      <div className={cls.infoCont}>
        <span>NEWS</span>
        <p>{el.title}</p>
      </div>
    </div>
  );
}
