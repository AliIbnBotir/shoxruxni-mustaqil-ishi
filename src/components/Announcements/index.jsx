import announcements from "../data/announcements";
import cls from "./announcements.module.scss";
import Card from "./card";
export default function Announcements() {
  const cards = announcements();
  return (
    <div className={cls.AnnouncementsCont}>
      <div className={cls.title}>
        <h1>Eng so'ngi joylashtirilgan e'lonlar</h1>
      </div>
      <div className={cls.CardsCont}>
        {cards.map((el) => (
          <Card el={el} />
        ))}
      </div>
    </div>
  );
}
