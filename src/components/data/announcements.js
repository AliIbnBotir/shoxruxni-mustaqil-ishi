import car1 from "../../assets/images/cars/1-full.jpg";
import car2 from "../../assets/images/cars/209.750x0.jpg";
import car3 from "../../assets/images/cars/28e64571073e3dff56e9fd1e24efca24.jpg";
import car4 from "../../assets/images/cars/9970183_0_0_1200_676_600x0_80_0_0_cb4a467d0df282c34909a153d292fa43.jpg";
import car5 from "../../assets/images/cars/9e2b4d5s-1920.jpg";
import car6 from "../../assets/images/cars/b93d83a57593fb5f110eb3bffc873abb-800x800.jpg";
import car7 from "../../assets/images/cars/EnginePower2.jpg";
import car8 from "../../assets/images/cars/Gw4-e9RujgKTYQjoqcGKRWpz4I0-1920.jpg";

function announcements() {
  return [
    {
      id: 1,
      title:
        "Lorem ipsum is placeholder text commonly used in the graphic, print.",
      img: <img alt="car" src={car1} />,
    },
    {
      id: 2,
      title:
        "Lorem ipsum is placeholder text commonly used in the graphic, print.",
      img: <img alt="car" src={car2} />,
    },
    {
      id: 3,
      title:
        "Lorem ipsum is placeholder text commonly used in the graphic, print.",
      img: <img alt="car" src={car3} />,
    },
    {
      id: 4,
      title:
        "Lorem ipsum is placeholder text commonly used in the graphic, print.",
      img: <img alt="car" src={car4} />,
    },
    {
      id: 5,
      title:
        "Lorem ipsum is placeholder text commonly used in the graphic, print.",
      img: <img alt="car" src={car5} />,
    },
  ];
}

export default announcements;
