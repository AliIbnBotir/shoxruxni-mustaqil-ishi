import car1 from "../../assets/images/cars/1-full.jpg";
import car2 from "../../assets/images/cars/209.750x0.jpg";
import car3 from "../../assets/images/cars/28e64571073e3dff56e9fd1e24efca24.jpg";
import car4 from "../../assets/images/cars/9970183_0_0_1200_676_600x0_80_0_0_cb4a467d0df282c34909a153d292fa43.jpg";
import car5 from "../../assets/images/cars/9e2b4d5s-1920.jpg";
import car6 from "../../assets/images/cars/b93d83a57593fb5f110eb3bffc873abb-800x800.jpg";
function bannerData() {
  return [
    {
      id: 1,
      img: (
        <div className="mainBannerImgCont">
          <img alt="car" src={car1} />
        </div>
      ),
    },
    {
      id: 2,
      img: (
        <div className="mainBannerImgCont">
          <img alt="car" src={car2} />
        </div>
      ),
    },
    {
      id: 3,
      img: (
        <div className="mainBannerImgCont">
          <img alt="car" src={car3} />
        </div>
      ),
    },
    {
      id: 4,
      img: (
        <div className="mainBannerImgCont">
          <img alt="car" src={car4} />
        </div>
      ),
    },
    {
      id: 5,
      img: (
        <div className="mainBannerImgCont">
          <img alt="car" src={car5} />
        </div>
      ),
    },
    {
      id: 6,
      img: (
        <div className="mainBannerImgCont">
          <img alt="car" src={car6} />
        </div>
      ),
    },
  ];
}

export default bannerData;
