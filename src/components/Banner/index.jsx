import Slider from "react-slick";
import bannerData from "../data/bannerImageData";
import cls from "./banner.module.scss";
export default function MainBanner({ setDescription }) {
  const settings = {
    dots: false,
    arrows: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    speed: 2000,
    autoplaySpeed: 5000,
   
    // cssEase: "linear"
  };
  const slides = bannerData();
  return (
    <div className={cls.slickContainer}>
      <Slider {...settings}>{slides.map((slide) => slide.img)}</Slider>
    </div>
  );
}
