import { Facebook, Instagram, LinkedIn, Twitter } from "@mui/icons-material";
import Container from "../Container";
import cls from "./footer.module.scss";

export default function Footer() {
  return (
    <div className={`${cls.FooterContainer} global-footer`}>
      <footer>
        <Container className={cls.footerCont}>
          <div className={cls.footerEl}>
            <h4>Xarid va Sotuv</h4>
            <p>Avtomobil toping</p>
            <p>Diler toping</p>
            <p>Shahar bo'yicha ro'yxatlar</p>
            <p>Sertifikatlangan oldindan egalik</p>
            <p>Avtomobil uchun to'lov kalkulyatorlari</p>
            <p>Avtomobil sharhlari va reytinglari</p>
            <p>Yonma-yon solishtiring</p>
            <p>Firibgarlikdan xabardorlik</p>
            <p>Avtomobilingizni Soting</p>
          </div>
          <div className={cls.footerEl}>
            <h4>Bizning kompaniya</h4>
            <p>MPCar.com haqida</p>
            <p>MPCar.com bilan bog'laning</p>
            <p>Investorlar bilan aloqalar</p>
            <p>Karyera</p>
            <p>Litsenziyalash va qayta chop etish</p>
            <p>Sayt xaritasi</p>
            <p>Fikr-mulohaza</p>
          </div>
          <div className={cls.footerEl}>
            <h4>Bizlar bilan bog'laning</h4>
            <div className={cls.specialLinks}>
              <div className={`${cls.media} ${cls.twitter}`}>
                <Twitter />
              </div>
              <div className={`${cls.media} ${cls.facebook}`}>
                <Facebook />
              </div>
              <div className={`${cls.media} ${cls.instagram}`}>
                <Instagram />
              </div>
              <div className={`${cls.media} ${cls.linkedin}`}>
                <LinkedIn />
              </div>
            </div>
          </div>
        </Container>
      </footer>
    </div>
  );
}
