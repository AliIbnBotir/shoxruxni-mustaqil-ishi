import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import cls from "./headerForm.module.scss";
export default function HeaderForm() {
  return (
    <div className={cls.headerForm}>
      <h2>Qidirish:</h2>
      <div className={cls.formInputCont}>
        <FormControl
          sx={{
            m: 0,
            backgroundColor: "#fff",
            border: "none",
            width: "100%",
          }}
        >
          <InputLabel>Yangi/Ishlatilgan</InputLabel>
          <Select
            fullWidth
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label="Yangi/Ishlatilgan"
          >
            <MenuItem value={10}>Yangi</MenuItem>
            <MenuItem value={20}>Ishlatilgan</MenuItem>
          </Select>
        </FormControl>
      </div>
      <div className={cls.formInputCont}>
        {" "}
        <FormControl sx={{ m: 0, backgroundColor: "#fff", width: "100%" }}>
          <InputLabel>Mashina turi</InputLabel>
          <Select
            fullWidth
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label="Mashina turi"
          ></Select>
        </FormControl>
      </div>
      <div className={cls.formInputCont}>
        {" "}
        <FormControl sx={{ m: 0, backgroundColor: "#fff", width: "100%" }}>
          <InputLabel>Narxi</InputLabel>
          <Select
            fullWidth
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label="Narxi"
          ></Select>
        </FormControl>
      </div>
      <div className={cls.formInputCont}>
        {" "}
        <FormControl sx={{ m: 0, backgroundColor: "#fff", width: "100%" }}>
          <InputLabel>Yurgan yo'li</InputLabel>
          <Select
            fullWidth
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label="Yurgan yo'li"
          ></Select>
        </FormControl>
      </div>
      <div className={cls.formInputCont}>
        {" "}
        <FormControl sx={{ m: 0, backgroundColor: "#fff", width: "100%" }}>
          <InputLabel>Manzil</InputLabel>
          <Select
            fullWidth
            labelId="demo-simple-select-label"
            id="demo-simple-select"
            label="Manzil"
          ></Select>
        </FormControl>
      </div>
      <div className={cls.formInputCont}>
        {" "}
        <Button
          style={{ height: "100%" }}
          variant="contained"
          color="primary"
          fullWidth
        >
          {" "}
          Qidirish{" "}
        </Button>
      </div>
    </div>
  );
}
