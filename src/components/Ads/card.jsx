import { Link } from "react-router-dom";
import cls from "./ads.module.scss";
export default function Card({el}) {
  return (
    <div className={cls.Card}>
      {el.img}
      <div className={cls.infoCont}>
        <p className={cls.title}>{el.title}</p>
        <p>{el.desc}</p>
        <Link to="#">{el.link}</Link>
      </div>
    </div>
  );
}
