import cls from "./ads.module.scss";
import img1 from "../../assets/images/mobile-app-vdp-d0cbbee38904cd66b17bd37f6a8b9424.png";
import img2 from "../../assets/images/electric-car-charging-e16e0c04bdbf317ec59ec5d0103acbaf.png";
import img3 from "../../assets/images/promo-pin-art-4de120d16ffab86177d031355ec8a2aa.png";
import Card from "./card";

export default function Ads() {
  const cardsData = [
    {
      title: "Lorem ipsum dolor sit amet",
      desc: "Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
      link: "quis nostrud",
      img: <img alt="card-img" src={img1} />,
    },
    {
      title: "Lorem ipsum dolor sit amet",
      desc: "Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
      link: "quis nostrud",
      img: <img alt="card-img" src={img2} />,
    },
    {
      title: "Lorem ipsum dolor sit amet",
      desc: "Consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
      link: "quis nostrud",
      img: <img alt="card-img" src={img3} />,
    },
  ];

  return (
    <div className={cls.AdsCont}>
      {cardsData.map((el) => (
        <Card el={el} />
      ))}
    </div>
  );
}
