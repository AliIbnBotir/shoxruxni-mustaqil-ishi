import { Person, PersonOutlined } from "@mui/icons-material";
import { Link } from "react-router-dom";
import Container from "../Container";
import cls from "./navbar.module.scss";

export default function Navbar() {
  return (
    <div className={`${cls.NavbarContainer} global-navbar`}>
      <nav>
        <div className={cls.logoContainer}>
          <Link to="/home">MPCar</Link>
        </div>
        <Container className={cls.navCont}>
          <ul>
            <li>
              <Link to="/home">Bosh sahifa</Link>
            </li>
            <li>
              <Link to="/heavy-trucks">Og'ir avtomobillar</Link>
            </li>
            <li>
              <Link to="/home">Yengil Avtomobillar</Link>
            </li>
            <li>
              <Link to="/home">Skuterlar</Link>
            </li>
            <li>
              <Link to="/home">Ma'lumot</Link>
            </li>
            <li>
              <Link to="/home">Biz bilan bog'lanish</Link>
            </li>
          </ul>
        </Container>
        <div className={cls.navbarEndCont}>
          <div className={cls.loginButton}>
            <PersonOutlined fontSize="large" />
            Kirish
          </div>
        </div>
      </nav>
    </div>
  );
}
