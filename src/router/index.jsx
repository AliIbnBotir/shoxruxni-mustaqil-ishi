import { Navigate, Route, Routes } from "react-router-dom";
import MainLayout from "../layouts/MainLayout";
import HeavyTrucks from "../views/heavyTrucks";
import HomePage from "../views/homePage";

const Router = () => {
  return (
    <Routes>
      <Route path="/" element={<MainLayout />}>
        <Route index element={<Navigate to="/home" />} />
        <Route path="home" element={<HomePage />} />
        <Route path="heavy-trucks" element={<HeavyTrucks />} />
        {/* <Route path="*" element={<Navigate to="/home" />} /> */}
      </Route>
    </Routes>
  );
};

export default Router;
