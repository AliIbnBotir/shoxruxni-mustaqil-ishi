import {
  Button,
  FormControl,
  Grid,
  InputBase,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import { useState } from "react";

import { styled } from "@mui/material/styles";
import { Link } from "react-router-dom";
import MainBanner from "../../components/Banner";
import Container from "../../components/Container";
import cls from "./homePage.module.scss";
import HeaderForm from "../../components/HeaderForm";
import Announcements from "../../components/Announcements";
import Ads from "../../components/Ads";
import Reviews from "../../components/Reviews";
const BootstrapInput = styled(Select)(({ theme }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },
  "& .MuiInputBase-input": {
    borderRadius: 0,
    position: "relative",
    backgroundColor: theme.palette.background.paper,
    border: "1px solid #ced4da",
    fontSize: 16,
    padding: "10px 26px 10px 12px",
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      "-apple-system",
      "BlinkMacSystemFont",
      '"Segoe UI"',
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
    "&:focus": {
      borderRadius: 4,
      borderColor: "#80bdff",
      boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)",
    },
  },
}));
export default function HomePage() {
  const [description, setDescription] = useState("");
  return (
    <Container className={cls.HomePageCont}>
      <div className={cls.headerCont}>
        <MainBanner setDescription={setDescription} />
        <div className={cls.infoCont}>
          <h1 className={cls.title}>Top Brendlar</h1>
          <div className={cls.description}>
            <p>
              At vero eos et accusamus et iusto odio dignissimos ducimus qui
              blanditiis praesentium voluptatum deleniti atque corrupti quos
              dolores et
            </p>
          </div>
        </div>
        <HeaderForm />
      </div>
      <Announcements />
      <Ads />
      <Reviews />
    </Container>
  );
}
