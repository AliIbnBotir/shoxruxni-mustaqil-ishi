import { Outlet } from "react-router-dom";
import Footer from "../../components/Footer";
import Navbar from "../../components/Navbar";
import styles from "./style.module.scss";

const MainLayout = () => {
  return (
    <div className={styles.layout}>
      <div className={styles.content}>
        <Navbar />
        <Outlet />
        <Footer />
      </div>
    </div>
  );
};

export default MainLayout;
